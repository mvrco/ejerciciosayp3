#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Persona{
    
    int edad;
    int telefono;
    char nombre[25];
    char mail[50];
    char direccion[25];
    
};

struct Persona crearPersona(int edad, int telefono,char nombre[], char mail[], char direccion[]){
        struct Persona persona;
        strcpy(persona.nombre, nombre);
        strcpy(persona.direccion, direccion);
        strcpy(persona.mail, mail);
        persona.telefono = telefono;
        persona.edad = edad;
        return persona;
};

void mostrarInformacion(struct Persona persona){

    printf("El nombre es: %s\n", persona.nombre);
    printf("El telefono es: %d\n", persona.telefono);
    printf("La edad es: %d\n", persona.edad);
    printf("El mail es: %s\n", persona.mail);
    printf("La direccion es: %s\n", persona.direccion);

};

void modificarInformacion(struct Persona *persona,int edad, int telefono,char nombre[], char mail[], char direccion[]){
    
    persona->edad = edad;
    persona->telefono = telefono;
    strcpy(persona->nombre, nombre);
    strcpy(persona->mail,mail);
    strcpy(persona->direccion,direccion);
    

};

int main() {
    int opcion = 0;
    struct Persona personas[5];
    int numPersonas = 0; 
    do{
        printf("Menu:\n");
        printf("1) Crear persona\n");
        printf("2) Ver datos de la persona\n");
        printf("3) Modificar informacion \n");
        printf("4) Salir\n");
        printf("Elija una opcion: \n");
        scanf("%d", &opcion);
        system("cls");
        switch(opcion) {
            case 1:
                if (numPersonas < sizeof(personas) / sizeof(personas[0])) {
                    int edad, telefono;
                    char nombre[25], mail[50], direccion[25];
                    printf("Ingresar edad: ");
                    scanf("%d", &edad);
                    printf("Ingresar telefono: ");
                    scanf("%d", &telefono);
                    printf("Ingresar nombre: ");
                    scanf("%s", nombre);
                    printf("Ingresar mail: ");
                    scanf("%s", mail);
                    printf("Ingresar direccion: ");
                    scanf("%s", direccion);

                    struct Persona p = crearPersona(edad, telefono, nombre, mail, direccion);
                    personas[numPersonas] = p;
                    numPersonas++;

                    printf("Persona creada exitosamente.\n");
                } else {
                    printf("No hay espacio disponible para crear una nueva persona.\n");
                }
                break;

            case 2:
                printf("Lista de personas, elegir su indice para mostrar la informacion:\n");
                for (int i = 0; i < sizeof(personas)/sizeof(personas[0]); i++) {
                    printf("%d) %s\n", i+1, personas[i].nombre);
                }
                int indice;
                printf("Ingrese el indice: ");
                scanf("%d", &indice);
                mostrarInformacion(personas[indice-1]);
                break;
            
            case 3:
                printf("Lista de personas, elegir su indice para modificar la informacion:\n");
                int i;
                for (i = 0; i < sizeof(personas)/sizeof(personas[0]); i++) {
                    printf("%d) %s\n", i+1, personas[i].nombre);
                }
                
                printf("Ingrese el indice: ");
                scanf("%d", &indice);
                int edad2, telefono2;
                char nombre2[25], mail2[50], direccion2[25];
                printf("Ingresar edad: ");
                scanf("%d",&edad2);
                printf("Ingresar telefono: ");
                scanf("%d",&telefono2);
                printf("Ingresar nombre: ");
                scanf("%s",nombre2);
                printf("Ingresar direccion: ");
                scanf("%s",direccion2);
                printf("Ingresar mail: ");
                scanf("%s",mail2);
                modificarInformacion(&personas[indice-1],edad2,telefono2,nombre2,direccion2,mail2);
                
                break;
            case 4:
                printf("Ha salido del menu.\n");
                exit(0);
                break;                
            }  
    }while(opcion != 4);
    system("Pause");
    return 0;
}

