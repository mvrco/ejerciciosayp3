#include <stdio.h>
#include <malloc.h>

typedef struct structNodo {
    int valor;
    struct structNodo *proximo;
} Nodo;

Nodo* crearLista() {
    Nodo *lista = NULL;
    return lista;
}

Nodo* agregarElemento(Nodo *lista, int valor) {
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;
    if (lista == NULL) {
        lista = nodoNuevo;
    } else {
        Nodo *cursor = lista;
        while (cursor->proximo != NULL) {
            cursor = cursor->proximo;
        }
        cursor->proximo = nodoNuevo;
    }
    return lista;
}

int agregarElementoOrdenado(Nodo **lista, int valor) {
    Nodo *nodoNuevo = malloc(sizeof(Nodo));
    nodoNuevo->valor = valor;
    nodoNuevo->proximo = NULL;

    if (*lista == NULL) {
        *lista = nodoNuevo;
        return 0;
    } else if ((*lista)->valor > valor) {
        nodoNuevo->proximo = *lista;
        *lista = nodoNuevo;
        return 0;
    } else {
        Nodo *cursor = *lista;
        while (cursor->proximo != NULL && cursor->proximo->valor < valor) {
            cursor = cursor->proximo;
        }
        nodoNuevo->proximo = cursor->proximo;
        cursor->proximo = nodoNuevo;
        return 0;
    }
}

int eliminarElemento(Nodo **lista, int valor) {
    Nodo *cursor = *lista;
    Nodo *anterior = NULL;
    int encontrado = 0;
    while (cursor != NULL) {
        if (cursor->valor == valor) {
            if (anterior == NULL) {
                *lista = cursor->proximo;
            } else {
                anterior->proximo = cursor->proximo;
            }
            free(cursor);
            printf("Se elimino el elemento con valor %d\n", valor); 
            encontrado = 1;
            break;
        }
        anterior = cursor;
        cursor = cursor->proximo;
    }

    if (!encontrado) {
        printf("No se encontro el elemento con valor %d, por lo tanto no se elimino \n", valor);
    } 

    return encontrado;
}

void obtenerElemento(Nodo *lista,int posicion){
    int encontrado = 0;
    Nodo *cursor = lista;
    int contador = 1;
    while (cursor != NULL) {
        if(contador == posicion){
            printf("En la posicion %d esta el valor %d \n",posicion, cursor->valor);
            encontrado = 1;
        }
        cursor = cursor->proximo;
        contador++;
    }
    if (1 != encontrado) {
        printf("En la posicion %d no hay elemento\n",posicion);
    }
}

int obtenerLargo(Nodo *lista) {
    int contador = 0;
    Nodo *cursor = lista;
    while (cursor != NULL) {
        contador++;
        cursor = cursor->proximo;
    }
    printf("Largo de la lista: %d\n", contador);
}
void imprimir(Nodo *lista) {
    Nodo *cursor = lista;
    while (cursor != NULL) {
        printf("El valor es %d\n", cursor->valor);
        cursor = cursor->proximo;
    }
}

int main() {

    Nodo *lista = crearLista();
    agregarElementoOrdenado(&lista, 3);
    agregarElementoOrdenado(&lista, 5);
    agregarElementoOrdenado(&lista, 4);
    agregarElementoOrdenado(&lista, 1);
    imprimir(lista);
    obtenerLargo(lista);
    obtenerElemento(lista,3);
    eliminarElemento(&lista,8);
    eliminarElemento(&lista,3);
    imprimir(lista);
    system("Pause");
    return 0;
}