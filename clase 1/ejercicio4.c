#include <stdio.h>

int main() {
    int num1, num2, num3;
    float prom;
    
    printf("Ingrese el primer numero: ");
    scanf("%d", &num1);
    
    printf("Ingrese el segundo numero: ");
    scanf("%d", &num2);
    
    printf("Ingrese el tercer numero: ");
    scanf("%d", &num3);
    
    prom = (num1 + num2 + num3) / 3.0;
    
    printf("El promedio es: %f \n", prom);
    system("Pause");
    
    return 0;
}