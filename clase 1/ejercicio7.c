#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main() {
    int opcion;

    do {
        printf("\n  Menu \n");
        printf("1) Opcion 1\n");
        printf("2) Opcion 2\n");
        printf("3) Opcion 3\n");
        printf("4) Salir\n");
        scanf("%d", &opcion);

        switch(opcion) {
            case 1:
                printf("Selecciono la opcion 1.\n");
                break;
            case 2:
                printf("Selecciono la opcion 2.\n");
                break;
            case 3:
                printf("Selecciono la opcion 3.\n");
                break;
            case 4:
                printf("Ha salido del menu.\n");
                exit(0);
                break;
            default:
                printf("Opcion incorrecta. \n");
        }
    } while(true);

    return 0;
}