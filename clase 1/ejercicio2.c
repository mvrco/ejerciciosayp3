#include <stdio.h>

int main() {
    int cadena[] = {1,2,3,4,5,6,7,8,9};
    int max  = cadena[0];
    
    for( int x = 1; x< sizeof(cadena) / sizeof(cadena[0]); x++){
        if(cadena[x]>max){
            max=cadena[x];
        }
    }
    printf("El maximo es: %d \n",max);
    system("Pause");
    
    return 0;
}